package pq_org

type User struct {
	Id       string
	Name     string
	Nickname string
	Emails   []string
}

func (u User) CreateUser() error {
	return nil
}
