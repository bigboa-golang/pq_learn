package pq_org

import (
	"database/sql"
	"errors"
	"github.com/lib/pq"
)

func InitDB() (*sql.DB, error) {
	connStr := "postgres://postgres:uDf7w5LF6IdHaYq@172.28.128.44/pq_learn01?sslmode=disable"
	connector, err := pq.NewConnector(connStr)
	if err != nil {
		return nil, errors.New("ERROR while connect to DB: " + err.Error())
	}
	db := sql.OpenDB(connector)

	err = db.Ping()
	if err != nil {
		return nil, errors.New("ERROR while ping DB: " + err.Error())
	}
	return db, err
}
